from __future__ import print_function
import os
from collections import defaultdict

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing

from sklearn.model_selection import StratifiedShuffleSplit

#import all data
data_path = ['data']
filepathfaces = os.sep.join(data_path + ['faces.csv'])
filepathcolors = os.sep.join(data_path + ['colors.csv'])
filepathfull_parse = os.sep.join(data_path + ['full_parse.csv'])
filepathobjects = os.sep.join(data_path + ['.csv'])
facesdata = pd.read_csv(filepathfaces)
colorsdata = pd.read_csv(filepathcolors)
data = pd.read_csv(filepathfull_parse)
# objectdata = pd.read_csv(filepathobjects)
final_dataframe = pd.DataFrame()

#filter noisy data
data = data[(data['average_rating'] > 0) & (data['published'] <= 2021)]
genres = data["genre_0"].value_counts()
genres = genres[(genres > 50)]

replace_dict = {"UNKNOWN": 0,
                "VERY_UNLIKELY": 1,
                "UNLIKELY": 2,
                "POSSIBLE": 3,
                "LIKELY": 4,
                "VERY_LIKELY": 5,
                }


facesdata = facesdata.replace(replace_dict)

newbooks = pd.DataFrame()
#calculate average face expression
facedata = ['joy:', 'sorrow', 'anger', 'surprise', 'under_exposed','blurred', 'headwear', 'face_confidence']
#loop through all facial expressions and calculate the average emotion per face
for bookindex, bookrow in data.iterrows():
    bookrow = bookrow.append(pd.Series(0, index=facedata))
    isbn = bookrow['isbn']
    faces = facesdata[facesdata['isbn'] == isbn]
    numfaces = 0
    for faceindex, facerow in faces.iterrows():
        numfaces += 1
        bookrow[facedata] += facerow[facedata]

    #get average
    if(numfaces > 0):
        bookrow[facedata] = (bookrow[facedata] / numfaces)

    #add that wierd array thingy at the end to make sure the column names don't shuffle
    newbooks = newbooks.append(bookrow, ignore_index=True)[bookrow.index.tolist()]
    if bookindex % 100 == 0:
        print("parsed book {}".format(bookindex))

print("Done parsing faces")
        #calculate average face expression



# merged1 = full_parsedata.merge(facesdata, on=['isbn'], how='outer')
# merged = merged1.merge(colorsdata, on=['isbn'], how='outer')
newbooks['review_count'] = pd.to_numeric(newbooks['review_count'], errors='coerce')
newbooks['amount_of_pages'] = pd.to_numeric(newbooks['amount_of_pages'], errors='coerce')
newbooks = newbooks.fillna(0)

print (genres)

#Take each genre, generate its correlations and only take the 'average rating' part
#Add that to a new dataframe
for genre in genres.index:
    full_parsedata = newbooks[newbooks["genre_0"] == genre]

    subset = full_parsedata[[
        'average_rating', 'review_count',
        'amount_of_pages', 'published', 'first_published', 'amount_of_faces',
        'amount_of_objects', 'authorScore', 'titleScore', 'joy:', 'sorrow', 'anger', 'surprise', 'under_exposed',
         'blurred', 'headwear']]

    subset = subset.astype(float)

    def generatePlots(x):
        final_dataframe[genre + "_rating"] = x.corr()['average_rating'][1:]
        print("added {}".format(genre))

    generatePlots(subset)


#Generate the heatmpa of all genres and emotions
plt.subplots(figsize=(40, 30))
plt.yticks(rotation=0)
plt.tick_params(axis='both', which='major', labelbottom = True, bottom=True, top = True, labeltop=True)
graph = sns.heatmap(final_dataframe, annot=True, fmt='.1g', vmin=-1, vmax=1, cmap="YlGnBu", square=True)
# print("done building heatmap {}".format(heatmap_name))
graph.figure.savefig("{}.png".format("heatmap_per_genre"))

#generate heatmap of everything
plt.clf()
plt.yticks(rotation=0)
plt.tick_params(axis='both', which='major', labelbottom = True, bottom=True, top = True, labeltop=True)
graph = sns.heatmap(newbooks.corr(), annot=True, fmt='.1g', vmin=-1, vmax=1, cmap="YlGnBu", square=True)
# print("done building heatmap {}".format(heatmap_name))
graph.figure.savefig("{}.png".format("heatmap_everything"))
