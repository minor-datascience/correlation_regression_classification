This repository contains all the Python Notebooks which were written in Kaggle. It also contains some other scripts, for example to generate data visualizations or to generate some misc models.
